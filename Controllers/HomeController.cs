﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using dotnetCoreWeb.Middlewares;
using dotnetCoreWeb.Models;
using Microsoft.AspNetCore.Mvc;

namespace dotnetCoreWeb.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var user = new UserModel();
            return View(model: user);
        }
    }
}