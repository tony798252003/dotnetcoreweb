﻿using dotnetCoreWeb.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace dotnetCoreWeb.Extensions
{
    public static class CustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseFirstMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<FirstMiddleware>();
        }
    }
}